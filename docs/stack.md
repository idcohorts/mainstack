## Security
- Authentication: [oauth2-proxy](https://oauth2-proxy.github.io/oauth2-proxy/)
- Authorization: [OpenFGA](https://openfga.dev/)?
## Backend
- API Routing: [FastAPI](https://fastapi.tiangolo.com/)
## Frontend
- Templating: [Astro](https://astro.build/)
- Style: [Tailwind](https://tailwindcss.com/)
## Data
- Model & Codegen: [LinkML](https://linkml.io/)
- Validation: [Pydantic](https://docs.pydantic.dev/)
- Database (if needed): [PostgreSQL](https://www.postgresql.org/)
- Object Relational Model: [SQL Alchemy](https://www.sqlalchemy.org/)
## Testing
- Unit tests: [pytest](https://docs.pytest.org/)
- Frontend tests: [Playwright](https://playwright.dev/)
