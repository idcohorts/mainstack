##  package management

### No external Javascript sources
You should include libraries and assets in your project via a package manager and not link directly to external libraries. [See this video for the background and clarification.](https://www.youtube.com/watch?v=2xLawmYa_KY)

